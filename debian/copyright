Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: SBMLToolbox
Source: http://sourceforge.net/projects/sbml/files/SBMLToolbox/
Upstream-Contact: sbml-team@caltech.edu

Files: *
Copyright: 2005-2012 Sarah M. Keating
           2009-2012 California Institute of Technology, Pasadena, CA, USA
                     EMBL European Bioinformatics Institute (EBML-EBI), Hinxton, UK
           2006-2008 California Institute of Technology, Pasadena, CA, USA
                     University of Hertfordshire, Hatfield, UK
           2003-2005 California Institute of Technology, Pasadena, CA, USA
                     Japan Science and Technology Agency, Japan
                     University of Hertfordshire, Hatfield, UK
Upstream-Contact: SBML-TEAM mailto:sbml-team@caltech.edu
License: LGPL-2.1

Files: debian/*
Copyright: 2012 Ivo Maintz <ivo@maintz.de>
           2016-2018 Andreas Tille <tille@debian.org>
License: GPL-2.0+

License: GPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2.1
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".

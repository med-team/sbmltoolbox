#!/bin/sh
NAME="SBMLToolbox"
echo TB=\"/usr/share/$NAME\" > debian/$NAME.sh
echo 'TB_PATH="$TB:\' >> debian/$NAME.sh
find toolbox/* -type d | LC_ALL=C sort | sed -e 's/toolbox/\$TB/g' -e 's/.*/&:\\/' >> debian/$NAME.sh
echo \" >> debian/$NAME.sh
echo 'echo $OCTAVE_PATH | grep -q $TB || export OCTAVE_PATH="${TB_PATH}${OCTAVE_PATH}"' >> debian/$NAME.sh
echo 'echo $MATLABPATH | grep -q $TB || export MATLABPATH="${TB_PATH}${MATLABPATH}"' >> debian/$NAME.sh
